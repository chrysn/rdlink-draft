# rdlink: Resource Directory mediated link resolution using decentralized authorities

This is the working area for the individual Internet-Draft, "rdlink: Resource Directory mediated link resolution using decentralized authorities".

* [Editor's Copy](https://git@gitlab.com:chrysn.github.io/rdlink-draft/#go.draft-amsuess-t2trg-rdlink.html)
* [Individual Draft](https://tools.ietf.org/html/draft-amsuess-t2trg-rdlink)
* [Compare Editor's Copy to Individual Draft](https://git@gitlab.com:chrysn.github.io/rdlink-draft/#go.draft-amsuess-t2trg-rdlink.diff)

## Building the Draft

Formatted text and HTML versions of the draft can be built using `make`.

```sh
$ make
```

This requires that you have the necessary software installed.  See
[the instructions](https://github.com/martinthomson/i-d-template/blob/master/doc/SETUP.md).


## Contributing

See the
[guidelines for contributions](https://github.com/git@gitlab.com:chrysn/rdlink-draft/blob/master/CONTRIBUTING.md).
